import java.io.*;
import java.util.Scanner;

class Department
{
    String name, location;

    Department(String name, String location)
    {
        StringBuilder temp = new StringBuilder(name);
        temp.setLength(20);
        this.name = temp.toString();

        temp = new StringBuilder(location);
        temp.setLength(20);
        this.location = temp.toString();
    }
}

public class Main
{
    private int inputIntPositive(String message)
    {
        Scanner reader = new Scanner(System.in);
        int num = -1;
        boolean isNumber;
        do
        {
            try
            {
                System.out.print(message);
                num = reader.nextInt();
                isNumber = true;
            }
            catch (Exception e) { isNumber = false; }
            if (!isNumber) reader.nextLine();
        }
        while (!isNumber || num <= 0);

        return num;
    }

    private void pause()
    {
        Scanner reader = new Scanner(System.in);
        System.out.print("\n\nPress enter to continue...");
        reader.nextLine();
    }

    private String inputAny(String message)
    {
        Scanner reader = new Scanner(System.in);
        System.out.print(message);
        return reader.nextLine();
    }

    private void createCopyFile(File original, File copied)
    {
        try
        {
            InputStream in = new BufferedInputStream(new FileInputStream(original));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(copied));

            byte[] buffer = new byte[1024];
            int lengthRead;
            if ((lengthRead = in.read(buffer)) > 0)
            {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }
            in.close();
            out.close();
        }
        catch (Exception e)
        {
            if (e.toString().contains("java.io.FileNotFoundException"))
                System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
            else
                System.out.println("\n\n" + e.toString());
        }
    }

    private void readFile()
    {
        try
        {
            FileInputStream fileIn = new FileInputStream(new File("Departments.dat"));
            DataInputStream file = new DataInputStream(fileIn);

            while (file.available() > 0)
            {
                int id = file.readInt();
                if (id < 10)
                    System.out.print("0" + id + " | ");
                else
                    System.out.print(id + " | ");
                System.out.print(file.readUTF());
                System.out.print(" | ");
                System.out.print(file.readUTF());
                System.out.println();
            }
            fileIn.close();
            file.close();
        }
        catch (Exception e)
        {
            if (e.toString().contains("java.io.FileNotFoundException"))
                System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
            else
                System.out.println("\n\n" + e.toString());
        }
    }

    private void readFile(int id)
    {
        try
        {
            FileInputStream fileIn = new FileInputStream(new File("Departments.dat"));
            DataInputStream file = new DataInputStream(fileIn);

            while (file.available() > 0 && file.readInt() != id)
            {
                file.readUTF();
                file.readUTF();
            }

            if (id < 10)
                System.out.print("0" + id + " | ");
            else
                System.out.print(id + " | ");
            System.out.print(file.readUTF());
            System.out.print(" | ");
            System.out.print(file.readUTF());
            System.out.println();

            fileIn.close();
            file.close();
        }
        catch (Exception e)
        {
            if (e.toString().contains("java.io.FileNotFoundException"))
                System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
            else
                System.out.println("\n\n" + e.toString());
        }
    }

    private boolean departmentExists(int id)
    {
        boolean exists = false;
        try
        {
            FileInputStream fileIn = new FileInputStream(new File("Departments.dat"));
            DataInputStream file = new DataInputStream(fileIn);

            while (!(exists = file.readInt() == id) && file.available() > 0)
            {
                file.readUTF();
                file.readUTF();
            }
            fileIn.close();
            file.close();
        }
        catch (Exception e)
        {
            if (e.toString().contains("java.io.FileNotFoundException"))
                System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
        }
        return exists;
    }

    private void ex1()
    {
        try
        {
            FileOutputStream fileIn = new FileOutputStream(new File("Departments.dat"));
            DataOutputStream file = new DataOutputStream(fileIn);

            Department[] departments = {
                    new Department("Ricardo Department", "Japan, Tokyo"),
                    new Department("Beauty", "France, Paris"),
                    new Department("Music Studio", "Country, City"),
                    new Department("Legalization", "Russia, Moscow"),
                    new Department("Production Marketing", "Germany, Berlin"),
                    new Department("Minecraft Games", "Sweden, Stockholm"),
                    new Department("Clothing Area", "France, Again"),
                    new Department("Technology", "Japan, Osaka"),
                    new Department("Exercising", "Thailand, Bangkok"),
                    new Department("Dancing", "Spain, Barcelona")};

            for (int i = 0; i < departments.length; i++)
            {
                file.writeInt(i + 1);
                file.writeUTF(departments[i].name);
                file.writeUTF(departments[i].location);
            }
            System.out.println("\nFile successfully created!");
            fileIn.close();
            file.close();
        }
        catch (Exception e)
        {
            if (e.toString().contains("java.io.FileNotFoundException"))
                System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
            else
                System.out.println("\n\n" + e.toString());
        }
    }

    private void ex2(int depModify, String newName, String newLocation)
    {
        if (departmentExists(depModify))
        {
            try
            {
                File f = new File("Departments.dat");
                File tempf = new File("Departments.temp");
                if (tempf.createNewFile()) tempf.deleteOnExit();

                createCopyFile(f, tempf);

                System.out.print("[OLD]");
                readFile(depModify);

                DataInputStream fileRead = new DataInputStream(new FileInputStream(tempf));
                DataOutputStream file = new DataOutputStream(new FileOutputStream(f));

                int id;
                while (fileRead.available() > 0)
                {
                    if ((id = fileRead.readInt()) == depModify)
                    {
                        file.writeInt(id);
                        StringBuilder tempString = new StringBuilder(newName);
                        tempString.setLength(20);
                        file.writeUTF(tempString.toString());
                        tempString = new StringBuilder(newLocation);
                        tempString.setLength(20);
                        file.writeUTF(tempString.toString());
                        fileRead.readUTF();
                        fileRead.readUTF();
                    }
                    else
                    {
                        file.writeInt(id);
                        file.writeUTF(fileRead.readUTF());
                        file.writeUTF(fileRead.readUTF());
                    }
                }

                System.out.print("[NEW]");
                readFile(depModify);

                file.close();
                fileRead.close();
                tempf.deleteOnExit();
            }
            catch (Exception e)
            {
                if (e.toString().contains("java.io.FileNotFoundException"))
                    System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
                else
                    System.out.println("\n\n" + e.toString());
            }
        }
        else
            System.out.println("The Department of ID: " + depModify + " doesn\'t exist");
    }

    private void ex3(int depDelete)
    {
        if (departmentExists(depDelete))
        {
            try
            {
                File f = new File("Departments.dat");
                File tempf = new File("Departments.temp");
                if (tempf.createNewFile()) tempf.deleteOnExit();

                createCopyFile(f, tempf);

                DataInputStream fileRead = new DataInputStream(new FileInputStream(tempf));
                DataOutputStream file = new DataOutputStream(new FileOutputStream(f));

                int id;
                while (fileRead.available() > 0)
                {
                    if ((id = fileRead.readInt()) == depDelete)
                    {
                        fileRead.readUTF();
                        fileRead.readUTF();
                    }
                    else
                    {
                        file.writeInt(id);
                        file.writeUTF(fileRead.readUTF());
                        file.writeUTF(fileRead.readUTF());
                    }
                }

                file.close();
                fileRead.close();
            }
            catch (Exception e)
            {
                if (e.toString().contains("java.io.FileNotFoundException"))
                    System.out.println("\nPlease, create the file \'Departaments.dat\' first.");
                else
                    System.out.println("\n\n" + e.toString());
            }
        }
        else
            System.out.println("The Department of ID: " + depDelete + " doesn\'t exist");
    }

    private void showMenuOptions()
    {
        System.out.println("---------- Welcome to my program ----------\n\n" +
                "1.[create][cfile]- Create file \'Departments.dat\'.\n" +
                "2.[modify][mod]- Modify line of file \'Departments.dat\'.\n" +
                "3.[delete][del]- Delete line of file \'Departments.dat\'.\n" +
                "4.[read][r]- Read the whole file \'Departments.dat\'\n" +
                "5.[exit][e]- Exit application.\n");
    }

    private boolean getInputMenu(String userSelection)
    {
        switch (userSelection.toLowerCase())
        {
            case "1": case "create": case "cfile": ex1();
            break;
            case "2": case "modify":
            case "mod": ex2(inputIntPositive("Please, insert the ID of the dep to modify: "), inputAny("Please, insert the department new name: "), inputAny("Please, insert the department new location: "));
                break;
            case "3": case "delete": case "del": ex3(inputIntPositive("Write the id of the department to delete: "));
            break;
            case "4": case "read": case "r": readFile();
            break;
            case "5": case "exit": case "e": return true;
        }
        pause();
        return false;
    }

    private void menu()
    {
        boolean finished;
        do
        {
            showMenuOptions();
            finished = getInputMenu(inputAny("Select an option by [command] or number: "));
        }
        while (!finished);
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.menu();
    }
}